---
layout: markdown_page
title: "Registration Initiatives"
description: This page captures and summarizes 
canonical_path: "/direction/registration-initiatives"
---

- TOC
{:toc}

### Problem

Today GitLab lacks visibility into who our free self-managed users are. Users can simply follow the [self managed install steps](https://docs.gitlab.com/omnibus/installation/index.html) and be up and running with GitLab in minutes. This open-core model is a huge strategic advantage as it lowers the barrier to entry for our product. The downside is that users are not required to register or "sync" their instance with GitLab preventing us from knowing who they are, providing better support and GTM motions, and offering services that may enable them to get even more value out of GitLab. 

This problem has compounding effects as you think through the implications:

GitLab:
* Product lacks visibility into feature usage, adoption, usability issues, and defects. 
* Inefficent GTM motions from outreach and lead generation to sales and support motions upgrading a free self-managed instance to a paid plan. 

End Users & Customers:
* Limited support solutions for customers. 
* Inefficent upgrade experience if that user intends to start a trial or upgrade their instance to a paid tier. 
* Poor customer experience if they run into issues or struggle with adoption and configuration. 

### Potential Solutions (ongoing, and proposed)

We're looking into various ways to address this problem across GitLab. This section aims to collect and link off to those major inititaves as a SSoT. If you see anything missing from this list please open an MR. 

#### Cloud Licensing (ongoing)

As a piece of underlying infrastructure, we need to enable a way for a customer to "sync" a self managed instance with us. This new system's primary purpose will be to activate and sync paid self-managed licenses for new customers, but can be used to sync unpaid licenses.

The value to GitLab includes increased visibility into seat utilization along with improved efficiency for sales and support. To the customer, we can provide real-time visibility into an account's utilization and improved tools for user management.

[Epic](https://gitlab.com/groups/gitlab-org/-/epics/5140)

#### Registration Features (proposal)

Allow free self-managed instances that activate Cloud Licensing to access otherwise paid features. These features would add value to the customer's installation and give them a "preview" of what features they could access at a paid tier. 

[Epic](https://gitlab.com/groups/gitlab-org/-/epics/5515)
[Direction Page](#)

#### GitLab Plus (proposal)

Similar to Registration Features but provide access to some cloud-based services that add additional value to a users installation of GitLab. Some examples include:

* Backup (both your instances data, and configuration settings)
* Managed Updates
* Cloud Storage (related to backup)
* Mail and notification services
* Migration Assistance
* Cloud Runners
* Enhanced VSM analytics

[Epic](https://gitlab.com/groups/gitlab-org/-/epics/308)
[Direction Page](#)



