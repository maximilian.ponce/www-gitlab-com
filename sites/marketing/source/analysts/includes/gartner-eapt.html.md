---
layout: markdown_page
title: "Gartner Magic Quadrant for Enterprise Agile Planning Tools (EAPT) 2019"
description: "This page represents how Gartner views our enterprise agile planning tools capabilities in relation to the larger market in 2019."
canonical_path: "/analysts/gartner-eapt/"
---
## GitLab and the Gartner Magic Quadrant for Enterprise Agile Planning Tools (EAPT) 2019*
This page represents how Gartner views our enterprise agile planning tools capabilities in relation to the larger market and how we're working with that information to build a better product. It also provides Gartner with ongoing context for how our product is developing in relation to how they see the market.

![Gartner EAPT MQ 2019](/images/analysts/gartner-magic-quadrant.jpg){: .small}

This graphic was published by Gartner, Inc. as part of a larger research document and should be evaluated in the context of the entire document. The Gartner document is available upon request by clicking the link on this page.
{: .note .font-small}

### Gartner's key takeaways on the EAPT market at the time of publication:

According to Gartner's definition of the EAPT market and specifically this report, "This Magic Quadrant evaluates vendors of tools focused on the development of software through the use of agile methodologies. Overall, we see several trends driving procurement decisions for these tools:
- This Magic Quadrant evaluates vendors of tools focused on the development of software through the use of agile methodologies. Overall, we see several trends driving procurement decisions for these tools:
- Experience with enterprise agile frameworks such as the Scaled Agile Framework (SAFe), Large-Scale Scrum (LeSS), Disciplined Agile (DA), Nexus and Scrum at Scale is growing.
- Clients with a legacy mix of tools are examining their tool strategies with a view to managing licensing, controlling costs, and creating a well-integrated toolset that meets the needs of teams and leaders alike.
- Public sector and regulated industries, many of which have lagged behind in terms of agile adoption, are showing increased interest in developing enterprise-scale agile capabilities.
- The increasing use of DevOps and its extension into the business means that parallel strategic commitments to agile development and governance are required.
- The drive to provide continuous value has led enterprises to shift from a project-based software delivery approach to one that treats applications as products. The corresponding shift to product management places new demands on EAP tools."

Additionally, Gartner makes specific observations about the evolution and adoption of agile development methodologies, "Agile development methodologies are highly accelerated, iterative development processes. They create a need for tools that support:
- Monthly, weekly and even daily deliverables based on business outcomes
- Increased visibility into the flow of work
-  Requirements captured in epics, features, user stories and tasks
-  Collaborative and shift-left practices such as test-driven development and acceptance test automation"


### Gartner's observations about GitLab

In this report, GitLab is recognized as a “**visionary**." According to Gartner, "Visionaries have an established customer base, but they may encounter execution challenges as they both continue to grow and develop products with an ambitious vision."   Additionally, Gartner adds "They will be attractive to organizations with mature enterprise agile practices who are looking to use agile as part of their digital business strategy."

### The GitLab Enterprise Agile Planning vision

At GitLab, we believe in a world where everyone can contribute, and we're building agile planning capabilities into Gitlab so that developers, product managers, security, ops, and business leaders can collaborate, contribute, and deliver value for their users.  Specifically, we aim to enable all people in any size organization to:

- **Contribute and Innovate** on new ideas.
- **Organize** those ideas into transparent, multi-level work breakdown plans that cut across departments.
- **Track** the execution of these plans, adjusting them along the way as needed.
- **Collaborate** with team members, internal and external stakeholders, and executive sponsors, using the same set of single-source-of-truth artifacts in a single application.
- **Measure** the efficacy of the entire process, characterizing the flows with value streams, thus providing actionable insight for continuous improvement.

Regardless of your agile methodology ([Kanban](https://about.gitlab.com/stages-devops-lifecycle/issueboard/), [Scrum](https://about.gitlab.com/solutions/agile-delivery/), or [SAFe](https://about.gitlab.com/solutions/agile-delivery/scaled-agile/)), GitLab's flexible and powerful approach to agile planning can support your needs.  Join us and contribute to our [Agile Planning vision](https://about.gitlab.com/direction/plan/).


Gartner does not endorse any vendor, product or service depicted in its research publications, and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner’s research organization and should not be construed as statements of fact. Gartner disclaims all warranties, express or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose.
{: .note .font-small .margin-top40}

Gartner, Magic Quadrant for Enterprise Agile Planning Tools, 18 April 2019, Keith Mann, Mike West, Thomas Murphy, Nathan Wilson
{: .note .font-small .margin-top40}
