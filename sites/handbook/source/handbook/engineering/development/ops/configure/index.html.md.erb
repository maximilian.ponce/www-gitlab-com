---
layout: handbook-page-toc
title: Configure Group
description: "The Configure group is responsible configure stage of the DevOps lifecycle."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For an understanding of where this team is going, take a look at [the product](/direction/configure/)
vision.

As a member of the Ops Sub-department, you may also like to understand [our](/direction/ops/)
overall vision.

## Mission

The Configure group is responsible for developing Ops-focused features of GitLab
that relate to the "Configuration" and "Operations" stages of the DevOps
lifecycle. These refer to the configuration of infrastructure, and the running
of applications deployed via GitLab. 

Our mission is to make the everyday work of the Platform Engineer more pleasant and enable Software 
Developers and Application Operators to deploy to company infrastructures without barriers.

This team is currently building out more features for our Kubernetes integration,
including the [Auto DevOps](/product/auto-devops/) feature set, and making it easier for
GitLab users to make the most of Kubernetes and DevOps best practices.

As per the [product categories](/handbook/product/categories/), this team
is responsible for building out new feature sets that will allow
GitLab users to easily make use of the following modern DevOps practices:

- Auto DevOps
- Kubernetes Configuration
- Infrastructure as Code
- Serverless

We work collaboratively and transparently, and we will contribute as much of our
work as possible back to the open source community.

### Product Indicators

Value is measured by **the number of deployment pipelines going through configure categories's features (**either K8s, Auto DevOps, Serverless or IaC in general). This is a good approximation of customer value because GitLab users ultimately want either their infrastructure or something of their infrastructure deployed.

We break down this metric into input metrics by product category and might come up with a further breakdown of the metrics when necessary.

#### AARRR for Kubernetes Management

``` mermaid
classDiagram
  Acquistion --|> Activation
	Acquistion : Are users aware of the product or feature set?
	Acquistion: Measurement (Count of Operations/Kubernetes docs views)
  Activation --|> Retention
	Activation : Are users applying the feature?
	Activation: Measurement (Count of the number of the registered Kubernetes Agents with valid tokens)
  Retention --|> Revenue
	Retention : Are users applying the feature over time?
	Retention: Measurement (Count of the number of GitOps synchronization operations)
  Revenue --|> Referral
	Revenue : Are users paying for the features?
	Revenue: Measurement (# of paying users that have installed an agent / Total users that have installed an agent.)
  Referral --|> Acquistion
	Referral : Are users encouraging others to use the feature?
	Referral: Measurement (Count of pageviews on Kubernetes cluster documentation coming from referrals)
```

#### AARRR for Infrastructure as Code

We have two features that we follow to understand the health of our funnel:

- Terraform reports can be used by any Terraform user on GitLab, and is a lightweight integration of GitLab and Terraform
- GitLab Managed Terraform State provides the deepest Terraform integration experience available

The following graph shows the metrics defined for Terraform reports. We want to track the same reports for the GitLab Managed Terraform State.

``` mermaid
classDiagram
  Acquistion --|> Activation
	Acquistion : Are users aware of the product or feature set?
	Acquistion: Measurement (Count of pageview on Infrastructure as Code documentation)
  Activation --|> Retention
	Activation : Are users applying the feature?
	Activation: Measurement (Number of projects with Terraform reports in the last month)
  Retention --|> Revenue
	Retention : Are users applying the feature over time?
	Retention: Measurement (Number of Terraform reports generated in the last month)
  Revenue --|> Referral
	Revenue : Are users paying for the features?
	Revenue: Measurement ((# of paying users that have Terraform reports / Total users that have have Terraform reports.))
  Referral --|> Acquistion
	Referral : Are users encouraging others to use the feature?
	Referral: Measurement (Count of pageviews on Infrastructure as Code documentation coming from referrals)
```

### Contribution to GitLab

At GitLab we build a single application for the whole DevOps lifecycle. The Configure group contributes to this vision at two levels. Similarly to other groups, if the usage of our features increases (measured by Stage Monthly Active Users), then more people and organisations benefit from GitLab's offerings in our area. Moreover, we own a special product category, Auto DevOps. Through Auto DevOps, we aim to enable the usage of features in every DevOps stage, thus driving business critical metrics, like Stages per User and Stages per Organisation.

## Team Members

<%= direct_team(manager_role: 'Backend Engineering Manager, Configure') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Configure/, direct_manager_role: 'Backend Engineering Manager, Configure') %>

## Common Links

- [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1223426?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=group%3A%3Aorchestration)
- [How we structure our day](https://gitlab.com/gl-retrospectives/configure/issues/17)
- [General Slack channel](https://gitlab.slack.com/archives/s_configure)
- [Standup Slack channel](https://gitlab.slack.com/archives/s_configure_standup)
- [Social Slack channel](https://gitlab.slack.com/archives/s_configure_social)

Some dedicated Slack channels:

* Kubernetes Agent: [`f_kubernetes_agent`](https://gitlab.slack.com/archives/f_kubernetes_agent)
* Secrets management: [`f_secrets_management`](https://gitlab.slack.com/archives/f_secrets_management)
* Vault integration: [`f_vault_integration_secrets_mgmt`](https://gitlab.slack.com/archives/f_vault_integration_secrets_mgmt)
* Terraform backend: [`f_terraform_backend`](https://gitlab.slack.com/archives/f_terraform_backend)
* [Terraform provider](https://github.com/gitlabhq/terraform-provider-gitlab): [`terraform-provider`](https://gitlab.slack.com/archives/terraform-provider)
* Auto DevOps: [`f_autodevops`](https://gitlab.slack.com/archives/f_autodevops)

## Processes

### Roadmap

We review our roadmap on a monthly basis. The roadmap is [maintained in Mural](https://app.mural.co/t/gitlab2474/m/gitlab2474/1603196180832/a0fbe8a5eb174a4b5dda15b44728ecbc7a4b377d).

### Issue refinement

Refinement in the Configure group happens by focusing on a few issues every week. These issues are being prepared for discussion in the week before with the active involvement of the PM, UX and EM. Based only on the description of the issue, the EM decides if the issue has enough context to invite the development team to discuss it during the next week. We aim to propose two issues by end of day every Friday, so people with any time zone can start their week by looking at the "to be discussed" issues.

The `~configure::discussion-candidate` and the `~configure::discussion` labels are used to mark issues that are currently being prepared for discussion or are currently being discussed, respectively.

### Planning

We use planning issues to work out the planning for the next milestone asynchronously.

1. The PM opens up a new milestone planning issue at the start of every milestone, and lists the issues that we should be working on in priority order.
1. All stakeholders (PM, UX, Engineering) are invited to collaborate on the planning for the next milestone.
1. The Engineering Managers review the issues to determine whether there will be sufficient capacity to deal with all the issues listed.
1. EMs also add bugs to the milestone that should be worked, based on Priority and Severity labels.

Example planning issue: [https://gitlab.com/gitlab-org/configure/general/issues/6](https://gitlab.com/gitlab-org/configure/general/issues/6)

#### GitLab Terraform Provider

The GitLab Terraform Provider is managed by the Configure team. We are working together with the community and are supporting other GitLab departments to add new functionalities to the provider. Work planned on the provider is part of the planning issue.
For each milestone, we take on one PR/MR that we want to ship in the provider, and we collect a list of PRs and issues where we see active community work to support. Each month a dedicated engineer or the EM works on delivering the listed items.

### Scheduling

#### Feature development

Our goal is to move towards a continuous delivery model so the team completes tasks regularly, and keeps working off of a prioritized backlog of issues. We default to team members self-scheduling their work:

- The team has weekly meetings to discuss issue priorities and to do issue refinement.
- Team members self-assign issues from the [Configure issue board](https://gitlab.com/groups/gitlab-org/-/boards/1223426?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=group%3A%3Aorchestration) that are in the `workflow:ready for development` column.
- Once a team member has completed their assigned issues, they are expected to go to the group issue boards and assign themselves to the next unassigned `workflow:ready for development` issue.
- The issues on the board are in priority order based on importance (the higher they are on the list, the higher the priority). This order is set by the product manager.
- If all issues are assigned for the milestone, team members are expected to identify the next available issue to work on based on the team's work prioritization (see below).
- While backstage work is important, in the absence of specific prioritization, the team will have a bias towards working on `bug` or `feature` categorized issues.

#### Bug fixing and priortized work

In addition to the self-scheduling of feature development, the manager will from time to time assign bugs, or other work deemed important, directly to a team member.

#### Iteration

We rely heavily on iteration as part of our workflow and aim to deliver the smallest possible MVC in the shortest period of time.

We use the following practices to enhance iteration on our team:

- Promoting large issues to epics and isolating specific pieces into separate issues
- Delivering issues behind a feature flag to maximize merge request rate
- Develop quick POCs to vet functionality and brainstorm product/market fit
- README driven development

### MR reviews

Team members should use their best judgment to determine whether to assign the first review of an MR based on the DangerBot's suggestion or to someone else on the team. Some factors in making this decision may be:

- If there is known domain expert for the area of code, prefer assigning the initial review to them.
- Does the MR require a lot of context to understand what it is doing? Will it take a reviewer outside the team a long time to ramp up on that context?
- Does the MR require a lot of dev environment setup that team members outside the Configure stage are likely to not have?
- Is the MR part of a larger effort for which a specific team member already has all the context?

## Quality Processes

Maintaining a high standard of quality is a critical factor to delivering winning products.

Within the Configure team we use the following processes and best practices to ensure high quality.

1. We ensure each MR is accompanied with meaningful unit tests and integration tests.
1. For each major feature we develop and maintain End to End tests that run nightly and confirm no regressions have been introduced to critical paths.
1. On a weekly basis, we review our [Triage report](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#triage-reports) for bugs and regressions and take the appropriate action.
1. We review the [quality dashboard](https://app.periscopedata.com/app/gitlab/736012/Quality-Embedded-Dashboard) each milestone to track our long term progress at improving quality.

### Async Issue Updates
In order to optimize async collaboration we leverage issue updates to share progress completed on a specific issue or epic.
Weekly updates on progress and status will be added to the issues as a comment. A weekly update may be skipped if there was no progress. It's preferable to update the issue rather than the related merge requests, as those do not provide a view of the overall progress. A weekly async update should be added to epics, providing an overview of the progress across related issues.

The status comment should include what percentage complete the work is, the confidence of the person that their estimate is correct and, notes on what was done and/or if review has started. It could be good to include whether this is a front end or back end update if there are multiple people working on it. Finally, for each MR associated, please include an entry for each.

Examples:
```
## Async status update

Complete: 80%
Confidence: 90%
Notes: expecting to go into review tomorrow
Concern: ~backend
```

```
Issue status: 20% complete, 75% confident

MR statuses:
!11111 - 80% complete, 99% confident - docs update - need to add one more section
!21212 - 10% complete, 70% confident - api update - database migrations created, working on creating the rest of the functionality next
```


## Career Development and Promotions

We want every team member to be advancing in their Career Development.

We follow the Engineering Department [Career Development Framework](https://about.gitlab.com/handbook/engineering/career-development/).

As part of the framework we engage in the following process:

1. Quarterly career development conversations
1. Regular self-evaluation within a Career matrix
1. Complete a coaching template if available
  - Candidates for Senior Engineer should complete this [coaching template](https://docs.google.com/document/d/1k0FgVXiGvnmeS9OkCo_WYNh4XROZ6J3z-tlNmdpRpkk/edit?usp=sharing) first as a starting point.
1. Review the Development [department expected competencies](https://about.gitlab.com/handbook/engineering/career-development/matrix/engineering/development/)
1. Once 30% of next-level expected competencies are met, a Promotional document is assembled based on evidence within the Career matrix
1. Once a Promotional document is ready, it is submitted.

## How to work with us

### How to contribute to Auto DevOps

Read our [specific GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/master/doc/howto/kubernetes)
instructions as well as our [handbook entry](/handbook/engineering/development/ops/configure/autodevops/)
on what existing testing does and how to develop features for Auto DevOps.

### Useful links for contributing to Auto DevOps

- [Tips and Troubleshooting](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/howto/kubernetes/tips_and_troubleshooting.md)
- [Useful Commands](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/howto/kubernetes/useful_commands.md)
- [How to work with slow connections](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/howto/kubernetes/tips_and_troubleshooting.md#qa)
- [Enabling premium features for development purposes](https://license.gitlab.com/users/sign_in)
- [Thanos query for complete Auto DevOps pipelines](https://thanos-query.ops.gitlab.net/graph?g0.range_input=2d&g0.max_source_resolution=0s&g0.expr=sum(increase(auto_devops_pipelines_completed_total%7Benv%3D%22gprd%22%7D%5B6h%5D))%20by%20(status)&g0.tab=0)
