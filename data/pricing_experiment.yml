plans:
  - id: free
    level: 0
    title: Free
    description: A complete DevOps platform
    sub_description: For you
    monthly_price: 0
    features:
      - title: Spans the DevOps lifecycle
      - title: Bring your own GitLab CI runners
      - title: Deploy to any production environment
      - title: Includes free static websites
      - title: Get 400 CI/CD minutes per month
    links:
      hosted: https://gitlab.com/users/sign_up?test=capabilities
      self_managed: /install/?test=capabilities
    button_copy: Get started
  - id: silver-premium
    level: 2
    title: Premium
    description: Iterate faster, innovate together
    sub_description: For your team
    monthly_price: 19
    features:
      - title: Better code reviews
        subfeatures:
          - title: Code and productivity analytics
          - title: Efficient merge request reviews
          - title: Code quality reports
          - title: Merge trains
          - title: Multiple approvers
      - title: Project management
        subfeatures:
          - title: Roadmaps
          - title: Single-level epics
          - title: Issue weights
          - title: Multiple issue assignees
          - title: Issue Board milestone lists
      - title: Checks and balances
        subfeatures:
          - title: Protected environments
          - title: Push rules
          - title: Require approvals
          - title: Merge Request dependencies
          - title: Multi-project pipeline visualization
      - title: 24/7 support
        subfeatures:
          - title: 30-minute response times when production is unavailable
      - title: Get 10,000 CI/CD minutes per month
        subfeatures:
          - title: 2,400% more minutes than Free
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0fd5a840403015aa6d9ea2c46d6&test=capabilities
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0ff6145d07001614f0ca2796525&test=capabilities
    button_copy: Buy Premium for DevOps
  - id: gold-ultimate
    level: 3
    title: Ultimate
    description: Deliver better software faster with enterprise-ready security solutions
    sub_description: For your organization
    monthly_price: 99
    features:
      - title: Advanced security testing
        subfeatures:
          - title: Vulnerability management
          - title: Security dashboards
          - title: Dynamic application security testing
          - title: Dependency scanning
          - title: Container scanning
          - title: Project dependency lists
      - title: Portfolio management
        subfeatures:
          - title: Issue and epic health reporting
          - title: Portfolio-level roadmaps
          - title: Group and project insights
          - title: Contribution analytics
          - title: Multi-level epics
      - title: Compliance
        subfeatures:
          - title: Compliance dashboards
          - title: Requirements management
          - title: Import and export requirements
          - title: Quality management
          - title: External approvals
          - title: Dependency proxy
          - title: License compliance
      - title: Free guest users
        subfeatures:
          - title: Excluded from license count
      - title: Get 50,000 CI/CD minutes per month
        subfeatures:
          - title: 400% more minutes than Premium
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0fc5a83f01d015aa6db83c45aac&test=capabilities
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0fe6145beb901614f137a0f1685&test=capabilities
    button_copy: Buy Ultimate for DevSecOps
questions:
  - question: What are pipeline minutes?
    answer: >-
      Pipeline minutes are the execution time for your pipelines on our shared runners. Execution on your own runners
      will not increase your pipeline minutes count and is unlimited.
  - question: What happens if I reach my minutes limit?
    answer: >-
      If you reach your limits, you can [manage your CI/CD minutes usage](https://about.gitlab.com/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage),
      [purchase additional CI minutes](https://customers.gitlab.com/plans), or
      upgrade your account to Premium or Ultimate. Your own runners can still be used even if you reach your limits.
  - question: Does the minute limit apply to all runners?
    answer: >-
      No. We will only restrict your minutes for our shared runners. If you have a
      [specific runner setup for your projects](https://docs.gitlab.com/runner/), there is no limit to your build time
      on GitLab SaaS.
  - question: Can I acquire a mix of licenses?
    answer: >-
      No, all users in the group need to be on the same plan.
  - question: Are GitLab Pages included in the free plan?
    answer: >-
      Absolutely, GitLab Pages will remain free for everyone.
  - question: Can I import my projects from another provider?
    answer: >-
      Yes. You can import your projects from most of the existing providers, including GitHub and Bitbucket.
      [See our documentation](https://docs.gitlab.com/ee/user/project/import/index.html) for all your import
      options.
  - question: I already have an account, how do I upgrade?
    answer: >-
      Head over to [https://customers.gitlab.com](https://customers.gitlab.com), choose the plan that is right for you.
  - question: Do plans increase the minutes limit depending on the number of users in that group?
    answer: >-
      No. The limit will be applied to a group, no matter the number of users in that group.
  - question: How much space can I have for my repo on GitLab SaaS?
    answer: >-
      10GB per project.
  - question: Can I buy additional storage space for myself or my organization?
    answer: >-
      Yes, you can purchase additional storage for your group on the [GitLab customer portal](https://customers.gitlab.com/). 
  - question: Do you have special pricing for open source projects, educational institutions, or startups?
    answer: >-
      Yes! We provide free Ultimate licenses, along with 50K CI minutes/month, to qualifying open source projects, educational institutions, and startups. Find out more by visiting our [GitLab for Open Source](/solutions/open-source/), [GitLab for Education](/solutions/education/), and [GitLab for Startups](/solutions/startups/) program pages.
  - question: How does GitLab determine what future features fall into given tiers?
    answer: >-
      On this page we represent our [capabilities](/company/pricing/#capabilities) and those are meant to be filters on our [buyer-based open core](/company/pricing/#buyer-based-tiering-clarification) pricing model. You can learn more about how we make tiering decisions on our [pricing handbook](/handbook/ceo/pricing) page.
  - question: Where is SaaS hosted?
    answer: >-
      Currently we are hosted on the Google Cloud Platform in the USA
  - question: What features do not apply to GitLab SaaS?
    answer: >-
      Some features are unique to self-managed and do not apply to SaaS. You can find an up to date list [on our why GitLab SaaS page](/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/index.html).
  - question: What is a user?
    answer: >-
      User means each individual end-user (person or machine) of Customer and/or its Affiliates (including, without
      limitation, employees, agents, and consultants thereof) with access to the Licensed Materials hereunder.
  - question: Can I add more users to my subscription?
    answer: >-
      Yes. You have a few options. You can add users to your subscription any time during the subscription period. You
      can log in to your account via the [GitLab Customers Portal](https://customers.gitlab.com) and add more seats or by
      either contacting [renewals@gitlab.com](mailto:renewals@gitlab.com) for a quote. In either case, the cost will be
      prorated from the date of quote/purchase through the end of the subscription period. You may also pay for the
      additional licences per our true-up model.
  - question: The True-Up model seems complicated, can you illustrate?
    answer: >-
      If you have 100 active users today, you should purchase a 100 user subscription. Suppose that when you renew next
      year you have 300 active users (200 extra users). When you renew you pay for a 300 user subscription and you also
      pay the full annual fee for the 200 users that you added during the year.
  - question: How does the license key work?
    answer: >-
      The license key is a static file which, upon uploading, allows GitLab Enterprise Edition to run. During license
      upload we check that the active users on your GitLab Enterprise Edition instance doesn't exceed the new number of
      users. During the licensed period you may add as many users as you want. The license key will expire after one
      year for GitLab subscribers.
  - question: What happens when my subscription is about to expire or has expired?
    answer: >-
      You will receive a new license that you will need to upload to your GitLab instance. This can be done by following
      [these instructions](https://docs.gitlab.com/ee/user/admin_area/license.html).
  - question: What happens if I decide not to renew my subscription?
    answer: >-
      14 days after the end of your subscription, your key will no longer work and GitLab Enterprise Edition will not be
      functional anymore. You will be able to downgrade to GitLab Community Edition, which is free to use.
